#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max_name 20
#define max_number 20
#define max_type 20

char *adress_c="cars_file.bin";
char *adress_ct="temp_cars_file.bin";
char *adress_p="pieces_file.bin";
char *adress_m="maintenance_file.bin";
char *adress_mt="temp_maintenance_file.bin";
char *adress_mt2="temp2_maintenance_file.bin";

typedef struct ca
{
                 int number,phone,colour;
                 char name[max_name],type[max_type];
                }cars;

typedef struct pce
{
    char p_name[20];
    int p_n;
    int p_sum;
    int p_price;
}pieces;

typedef struct dat
{
    int d,m,y;
}date;

typedef struct maintenance
{
    int m_num,c_num,p_num;
    date t;
    int m_cost;
    int total_cost;
    int used_pieces[20][2];

} maint ;

typedef struct
{
    maint mainten;
    struct node *next;
}node;

typedef node* ptr_node;

//void insert(ptr_node*,maint)
//{
//    maint mant,v;
//    ptr_node phead=NULL;
//    ptr_node temp=NULL,curr,prev;
//    temp=malloc(sizeof(node));
//    temp->mainten=v;
//    temp->next=NULL;
//    if(*phead=NULL)
//        *phead=temp;
//    else
//        if(temp->mainten.c_num<(*phead.mainten.c_num)
//        {
//           temp->next=*phead;
//           *phead=temp;
//        }
//    else
//        {
//        curr=*phead;
//        prev=NULL;
//        while(curr!=NULL && temp->mainten.c_num>= curr->mainten.c_num)
//        {
//            prev=curr;
//            curr=curr->next;
//        }
//        prev->next=temp;
//        temp->next=curr;
//
//
// }
//
//}
//
//count =0;
//FILE *pm= fopen(adress_m,"rb");
//if(file_is_ok(pm))
//{
//    while(fread(&mant,sizeof(main,1,pm)!=NULL))
//    {
//        insert(&head,mant);
//        count++;
//
//    }
//
//}
//fclose(pm);



void creat_file();
int file_is_ok(FILE *fptr);
void display_file();
void display_car_maint(int num);
void add_procces();
void search_by_owner(char *owner);
int find_price(int num);
void add_a_car();
void add_piece();
char *print_colour(int c);
int delete_a_car(int number);
int delete_a_process(int number);
int SearchByNum(int num,int c);
void sort_by_num();
int count_cars(void);
int count_maint(void);
void copy_first_to_second(char *first,char *second,int c);
void wheels();
void most_used_piece();
void display_car_from__date(date n_date);
void display_car_maint(int num);


int file_is_ok(FILE *fptr)
{
    if(fptr==NULL)
    {
        printf("error in file creation");
        exit(-1);
    }
    else
        return 1;
}

void creat_file()
{
    FILE *pcar=fopen(adress_c,"ab");
    file_is_ok(pcar);
    FILE *pp=fopen(adress_p,"ab");
    file_is_ok(pp);
    FILE *pm=fopen(adress_m,"ab");
    file_is_ok(pm);
}

int count_cars(void)
{
    FILE *fp=fopen(adress_c,"rb");
    int counter=0;
    cars temp;
    while(fread(&temp,sizeof(cars),1,fp)==1)
            counter++;
    return counter;
}

int count_maint(void)
{
    FILE *fp=fopen(adress_m,"rb");
    int counter=0;
    maint temp;
    while(fread(&temp,sizeof(maint),1,fp)!=NULL)
            counter++;
    return counter;
}

int count_pieces(void)
{
    FILE *pp=fopen(adress_p,"rb");
    int counter=0;
    pieces temp;
    while(fread(&temp,sizeof(pieces),1,pp)!=NULL)
            counter++;
    return counter;
}

void copy_first_to_second(char *first,char *second,int c)
{
    FILE *pfirst=fopen(first,"rb");
    FILE *psecond=fopen(second,"wb");
    if(file_is_ok(pfirst)&&file_is_ok(psecond))
    {
        if(c==1)
        {
            cars temp;
            while( fread(&temp,sizeof(cars),1,pfirst)!=NULL)
                fwrite(&temp,sizeof(cars),1,psecond);
        }
        else if(c==2)
        {
            maint temp;
            while( fread(&temp,sizeof(maint),1,pfirst)!=NULL)
                fwrite(&temp,sizeof(maint),1,psecond);
        }
    }
    fclose(pfirst);
    fclose(psecond);
}


void sort_by_num()
{
    maint r_maint,min;
    FILE *pfile=fopen(adress_m,"rb");
    FILE *ptemp=fopen(adress_mt,"wb");

    if((file_is_ok(ptemp)) && (file_is_ok(pfile)) )
    {
        int i;
        while(fread(&r_maint,sizeof(maint),1,pfile) != NULL )
        {
            min = r_maint;
            rewind(pfile);
            while(fread(&r_maint,sizeof(maint),1,pfile)  != NULL )
             {
                if (min.m_num > r_maint.m_num )
                    {
                        min = r_maint;
                    }
             }
             fwrite(&min,sizeof(maint),1,ptemp);
             delete_a_process(min.m_num);
             rewind(pfile);
             }
    }
    fclose(pfile);
    fclose(ptemp);
    copy_first_to_second(adress_mt,adress_m,2);

}


int SearchByNum(int num,int c)
{
    if(c==1)
    {
        cars temp;
        int i=1;
        FILE *pfile=fopen(adress_c,"rb");
        if(file_is_ok(pfile))
            while(fread(&temp,sizeof(cars),1,pfile)!=NULL)
                if(temp.number==num)
                {
                    fclose(pfile);
                    return i;
                }
                else
                    i++;
        fclose(pfile);
        return -1;
    }
    else if(c==2)
    {
        FILE *pp=fopen(adress_p,"rb");
        if(file_is_ok(pp))
        {
            pieces temp;
            int i;
             while(fread(&temp,sizeof(pieces),1,pp)!=NULL)
                if(temp.p_n==num)
                {
                    fclose(pp);
                    return i;
                }
                else
                    i++;
        fclose(pp);
        return -1;
    }

        }



}

int delete_a_process(int number)
{
   int ok=0;
   maint temp;
   FILE *pfile=fopen(adress_m,"rb");
   FILE *ptemp=fopen(adress_mt2,"wb");
   if((file_is_ok(pfile)) && (file_is_ok(ptemp)))
       while(fread(&temp,sizeof(maint),1,pfile)!=NULL)
            {
               if((number!=temp.m_num&&!ok) ||  ok )
                    fwrite(&temp,sizeof(maint),1,ptemp);
               else
                    ok=1;
            }
    fclose(pfile);
    fclose(ptemp);
    copy_first_to_second(adress_mt2,adress_m,2);
    if (ok)
        return 1;
    return 0;
}

int delete_a_car(int number)
{
   int ok=0;
   cars temp;
   FILE *pfile=fopen(adress_c,"rb");
   FILE *ptemp=fopen(adress_ct,"wb");
   if(file_is_ok(pfile)&&file_is_ok(ptemp))
       while(fread(&temp,sizeof(cars),1,pfile)!=NULL)
            {
               if((temp.number!=number&&!ok) ||  ok )
                    fwrite(&temp,sizeof(cars),1,ptemp);
               else
                    ok=1;
            }
    fclose(pfile);
    fclose(ptemp);
    printf("**");
    copy_first_to_second(adress_ct,adress_c,1);
    if (ok)
        {
            delete_a_process(number);
            printf("car have been deleted..!");
            return 1;
        }
    return 0;
}

char *print_colour(int c)
{
 switch(c)
 {
 case 1:
     return "blue";
 case 2:
    return "red";
 case 3:
    return "black";
 case 4:
    return "yellow";
 case 5:
    return "green";
 case 6:
    return "purple";
 case 7:
    return "orange";
 case 8:
    return "white";
 case 9:
    return "pink";
 }
return "fail";
}

void add_piece()
{
    FILE *pp=fopen(adress_p,"ab");
    if(file_is_ok(pp))
    {
        pieces temp;
        pieces new_piece;
        char choice;
        do
        {
            int num_checker=0;
            do
            {
                printf("\nPiece Number :\t");
                scanf("%d",&new_piece.p_n);
                fflush(stdin);
                num_checker=SearchByNum(new_piece.p_n,2);
                if(num_checker > 1)
                    printf("\n %cThis number has been used before..!\n\t\tplease enter a new number.\n",15);
                if(new_piece.p_n < 0)
                    printf("\n %cInvalid car number..!\nplease enter a new number\n",15);
             }
            while((new_piece.p_n < 0 ) || (num_checker > 0));
            printf("Piece Name:\t");
            fflush(stdin);
            scanf("%s",&new_piece.p_name);
            fflush(stdin);
            printf("Piece price:\t");
            scanf("%d",&new_piece.p_price);
            fflush(stdin);
            printf("Piece quantity:\t");
            scanf("%d",&new_piece.p_sum);
            fflush(stdin);
            fwrite(&new_piece,sizeof(pieces),1,pp);
            rewind(pp);
            fflush(stdin);
            printf("\n %cKeep adding ? (Y)es/(N)o\nyour choice:\t",16);
            scanf("%c",&choice);
        }
        while((choice=='y')||(choice=='Y'));
        fclose(pp);

    }


}

void add_a_car()
{
    char choice;
    int num_checker;
    cars car;
    cars temp;
    FILE *pcar=fopen(adress_c,"rb+");
    rewind(pcar);
    num_checker=0;
    int tmp;
    if (file_is_ok(pcar))
    {
        do
        {
            do
            {

              printf("\nenter car number:\t");
              scanf("%d",&car.number);
              num_checker= SearchByNum(car.number,1);
             if(num_checker > 0 || car.number==tmp)
                    printf("\n This number has been used before..!\n\n");
             if(car.number < 0)
                    printf("\n Invalid car number..!\n\n",15);
             }
            while((car.number < 0 ) || (num_checker > 0) || car.number==tmp);
            tmp=car.number;
            printf(" %c Car type: ",16);
            fflush(stdin);
            gets(car.type);

            printf(" %c choose a colour from this menu please\nColours menu :\n--------------\n1- blue.\n2- red.\n3- black.\n4- yellow.\n5- green.\n6- purple.\n7- orange.\n8- white.\n9- pink.\nyour choice : ",16);
            fflush(stdin);
            scanf("%d",&car.colour);

            printf(" %c Car Owner: ",16);
            fflush(stdin);
            gets(car.name);
            do
            {
                printf(" %c phone number: ",16);
                fflush(stdin);
                scanf("%ld",&car.phone);
                if(car.phone < 0 )
                    printf("\n %c Invalid phone number..!\n\n",15);
            }
            while(car.phone < 0 );
            rewind(pcar);
            cars temp2;
            while((fread(&temp,sizeof(cars),1,pcar))!=NULL)
            {
                if(temp.number > car.number)
                {
                    temp2=temp;
                    fseek(pcar,sizeof(cars)*(-1),SEEK_CUR);
                    fwrite(&car,sizeof(cars),1,pcar);
                    fseek(pcar,sizeof(cars)*(-1),SEEK_CUR);
                    car=temp2;
                }
            }
            fwrite(&car,sizeof(cars),1,pcar);
            printf("\t\t\t**Car Have Been Added **\n");
            printf("\n Keep adding ? Y/N:   ");
            fflush(stdin);
            scanf("%c",&choice );
     }
       while((choice == 'y' ) || (choice == 'Y'));
       printf("information added successfully..!");
       fclose(pcar);


    }

}

int find_price(int num)
{
    FILE *pp=fopen(adress_p,"rb");
    if(file_is_ok(pp))
    {
        pieces temp;
        int c=0;
        while(fread(&temp,sizeof(pieces),1,pp)!=NULL)
        {
            if(temp.p_n==num)
            {
                c=temp.p_price;
                return c;
            }

        }
        printf("\n\t(NOT FOUND) ..the number of the piece is wrong..!");
        return 0;

    }
}

void search_by_owner(char *owner)
{
    int n;
    FILE *pcar=fopen(adress_c,"rb");
    if(file_is_ok(pcar))
    {
        cars temp;
        int i=1;
        while(fread(&temp,sizeof(cars),1,pcar)!=NULL)
        {
            i++;
            if(strcmp(temp.name,owner)==0)
            {
                rewind(pcar);
                i=0;
                break;
            }
        }
        if(i==0)
        {
            printf("The owner <%s> Has car(s) with the following number(s):\n",owner);
            while(fread(&temp,sizeof(cars),1,pcar)!=NULL)
            {
                if(strcmp(temp.name,owner)==0)
                {
                    printf("\t%c(%d)\n",16,temp.number);
                }
            }
        }
        else
        {
            printf("The Owner <%s> Has not found..! ",owner);
        }

    }


}

void most_used_piece()
{
    FILE *pmaint=fopen(adress_m,"rb");
    FILE *pp=fopen(adress_p,"rb");
    maint temp;
    pieces ptemp;
    int n=count_pieces();
    int a[n][2],i=0,j,c;

    if( file_is_ok(pmaint) && file_is_ok(pp))
    {
        while(fread(&ptemp,sizeof(pieces),1,pp)!=NULL)
        {
            a[i][0]=ptemp.p_n;
            a[i][1]=0;
            while(fread(&temp,sizeof(maint),1,pmaint)!=NULL)
            {
                for(j=0;j<temp.p_num;j++)
                {
                    if(temp.used_pieces[j][0]==ptemp.p_n)
                        a[i][1]+=temp.used_pieces[j][1];

                }
            }
            i++;

        }
        c=i;
        int b=0;
        int max=a[0][1];
        for(j=0;j<c;j++)
            if(max<a[j][1])
                {max=a[j][1];
                b=j;
                }
        printf("Pieces record:\n\n     NUMBER   \t      NAME     \t      PICE    \t     QUANTITY\t\t\n=================================================================\n");
        pieces temp2;
        rewind(pp);
        while(fread(&temp2,sizeof(pieces),1,pp)!=NULL)
            {
                            printf("0");

                if(temp2.p_n==a[b][0])
                    printf("|\t%d\t|\t%s\t|\t%d\t|\t%d\t|\n",temp2.p_n,temp2.p_name,temp2.p_price,(temp2.p_sum-a[b][1]));
            }
            printf("_________________________________________________________________");

        fclose(pp);

        }
}

void add_procces()
{
    int choice;
    char choice2;
    int num;
    int num_checker,c=0;
    FILE *pmaint=fopen(adress_m,"ab");
    if(file_is_ok(pmaint))
    {
        maint temp;
        do
        {
            printf("\nenter car number:\t");
            scanf("%d",&temp.c_num);
            num_checker = SearchByNum(temp.c_num,1);
            while(num_checker <  0)
           {
                printf("there's no car with the number you entered..! ");
                printf("\n#1 Enter a new number.\n#2 Enter a new car with this number. \n ");
                fflush(stdin);
                scanf("%d",&choice);
                if(choice==1)
                {
                    printf("enter your new number:\t");
                    fflush(stdin);
                    scanf("%d",&num);
                    num_checker=SearchByNum(num,1);
                }
                else if(choice==2)
                {
                    add_a_car(num);
                    num_checker=SearchByNum(num,1);
                }
            }
            if(num_checker > 0)
            {
                int pieces_cost=0;
                int total_cost=0;
                printf("\nenter process number:\t ");
                scanf("%d",&temp.m_num);
                do
                {printf("enter process date:\n\tday: ");
                scanf("%d",&temp.t.d);
                if((temp.t.d < 1) || (temp.t.d > 31))
                    printf("error..! renter the day.\n");
                }
                while((temp.t.d < 1) || (temp.t.d > 31));
                do
                {printf("\n\tmonth: ");
                scanf("%d",&temp.t.m);
                if((temp.t.m < 1) || (temp.t.m > 12))
                    printf("error..! renter the month.\n");
                }
                while((temp.t.m < 1) || (temp.t.m > 12));
                do
                {printf("\n\tyear: ");
                scanf("%d",&temp.t.y);
                if((temp.t.y < 1990) || (temp.t.d > 2018))
                    printf("error..! renter the year.\n");
                }
                while((temp.t.y < 1990) || (temp.t.d > 2018));
                printf("\nHow many type of pieces were used in the process ");
                scanf("%d",&temp.p_num);
                fflush(stdin);
                int i;
                for(i=0;i<temp.p_num;i++)
                {
                    do
                    {
                        printf("piece number :  ");
                        scanf("%d",&temp.used_pieces[i][0]);
                        fflush(stdin);
                        c=find_price(temp.used_pieces[i][0]);
                        if(c==0)
                            printf("the number is not exist , enter another one.\n");
                    }while(c==0);
                    fflush(stdin);
                    printf("\n quantity  :  ");
                    scanf("%d",&temp.used_pieces[i][1]);
                    fflush(stdin);

                    pieces_cost += ( c * temp.used_pieces[i][1] ) ;
                }
                printf("\nmaintinance costs : ");
                scanf("%d",&temp.m_cost);
                fflush(stdin);
                temp.total_cost = temp.m_cost + pieces_cost ;
                fseek(pmaint,sizeof(maint)*(0),SEEK_END);
                fwrite(&temp,sizeof(maint),1,pmaint);
                fflush(stdin);
            }
            printf("\n  Keep Adding?\n (y)es / (N)o  ");
                scanf("%c",&choice2);
                fflush(stdin);
        }
        while((choice2 == 'y' ) || (choice2 == 'Y'));
        fclose(pmaint);
        printf("information added successfully..!");
        sort_by_num();
    }
}

void display_car_by_num(int number)
{
    cars temp;
    int counter=0;
    FILE *pcar=fopen(adress_c,"rb");
    printf("\n--------------------\nNumber\t\tphone\t\towner\t\tcolour\t\ttype\n================================================================================\n",count_cars());
    while(fread(&temp,sizeof(cars),1,pcar)!=NULL)
    {
        if(temp.number==number)
        {
            printf("%d\t\t%d\t\t%s\t\t%s\t\t%s\t\t",temp.number,temp.phone,temp.name,print_colour(temp.colour),temp.type);
            printf("________________________________________________________________________________");
            break;
        }
    }
}


void display_car_from__date(date n_date)
{
    FILE *pm=fopen(adress_m,"rb");
    maint temp;
    int counter=0;
    if(file_is_ok(pm))
    {
        while(fread(&temp,sizeof(maint),1,pm)!=NULL)
        {
            if(temp.t.y > n_date.y)
                {
                    display_car_by_num(temp.c_num);
                    counter++;
                }
            else if(temp.t.y == n_date.y)
            {
                if(temp.t.m > n_date.m)
                {
                    display_car_by_num(temp.c_num);
                    counter++;
                }
                else if(temp.t.m==n_date.m)
                {
                    if(temp.t.d > n_date.d)
                    {
                        display_car_by_num(temp.c_num);
                        counter++;
                    }
                }
            }

        }
        if(counter==0)
            printf("there's no cars after this date..!\n");
    }
}





//void display_list(ptr_node head)
//{
//    int count=0;
//    if(head!=null)
//    {
//        ptr_node temp=head;
//           printf("\n--------------------\n |M_Number|  |  Date  |  |C_Number|  |Pieces|  |M_Cost| |Total Cost|\n================================================================================");
//
//        while(temp!=NULL)
//        {
//            printf("     %d\t     (%d/%d/%d)\t     %d\t        %d\t    %d\t    %d\n",temp.m_,temp.t.d,temp.t.m,temp.t.y,temp.c_num,temp.p_num,temp.m_cost,temp.total_cost);
//            printf("________________________________________________________________________________");
//
//            count++;
//        }
//    }
//}

void display_car_maint(int num)
{
    maint temp;
    int counter =0;
    int cost=0;
    FILE *pmaint=fopen(adress_m,"rb");int m_num,c_num,p_num;
    while(fread(&temp,sizeof(maint),1,pmaint)!=NULL)
    {
        if(temp.c_num == num)
        {
            printf("\nThe #%d process :\n",counter+1);
                printf("\n--------------------\n |M_Number|  |  Date  |  |C_Number|  |Pieces|  |M_Cost| |Total Cost|\n================================================================================");
                printf("     %d\t     (%d/%d/%d)\t     %d\t        %d\t    %d\t    %d\n",temp.m_num,temp.t.d,temp.t.m,temp.t.y,temp.c_num,temp.p_num,temp.m_cost,temp.total_cost);
                printf("________________________________________________________________________________");
                counter++;
                cost += temp.total_cost;
                int i;
                printf("\n Used pieces record:\n |NUMBER|  |QUANTITY| \n ==========================\n");
                for(i=0;i<temp.p_num;i++)
                {
                    printf(" |  %d   |  |   %d    | \n", temp.used_pieces[i][0],temp.used_pieces[i][1]);
                }
        }
    }
    printf("\ntotal cost = %d\n",cost);

}

void wheels()
{
    FILE *pm=fopen(adress_m,"rb");
    maint temp2;
    int i;
    while(fread(&temp2,sizeof(maint),1,pm)!=NULL)
    {
        for(i=0;i<temp2.p_num;i++)
            if(temp2.used_pieces[i][0]==1)
            {
                FILE *pcar=fopen(adress_c,"rb");
                rewind(pcar);
                cars temp1;
                while(fread(&temp1,sizeof(cars),1,pcar)!=NULL)
                {
                    if(temp1.number==temp2.c_num)
                    {
                        printf("\n\t(%s)\n",temp1.type);
                    }

                }
            }
    }

}


void display_file()
{
    int choice;
    printf("which record you would like to print ?\n1- cars records.\n2- maintenance records.\n3- pieces records. \nyour choice : ");
    scanf("%d",&choice);
    fflush(stdin);
    if(choice==1)
    {
        if(count_cars()==0)
            printf("\nThere is no cars in the Car file..!\n");
        else
        {
            cars temp;
            int counter=0;
            FILE *pcar=fopen(adress_c,"rb");
            printf("\nCARS FILE : %d car(s)\n--------------------\nNumber\t\tphone\t\towner\t\tcolour\t\ttype\n================================================================================",count_cars());
            while(fread(&temp,sizeof(cars),1,pcar)!=NULL)
            {
              printf("%d\t\t%d\t\t%s\t\t%s\t\t%s\t\t",temp.number,temp.phone,temp.name,print_colour(temp.colour),temp.type);
              printf("________________________________________________________________________________");
              counter++;
            }
         }
    }
    else if(choice==2)
    {
        if(count_maint()==0)
            printf("\nThere is no processes in the maintenance file..!\n");
        else
        {
            maint temp;
            int counter =0;
            FILE *pmaint=fopen(adress_m,"rb");
            printf("\nMaintenance File : %d process(es) \n",count_maint());

            while(fread(&temp,sizeof(maint),1,pmaint)!=NULL)
            {
                printf("\nThe #%d process :\n",counter+1);
                printf("\n--------------------\n |M_Number|  |  Date  |  |C_Number|  |Pieces|  |M_Cost| |Total Cost|\n================================================================================");
                printf("     %d\t     (%d/%d/%d)\t     %d\t        %d\t    %d\t    %d\n",temp.m_num,temp.t.d,temp.t.m,temp.t.y,temp.c_num,temp.p_num,temp.m_cost,temp.total_cost);
                printf("________________________________________________________________________________");
                counter++;
                int i;
                printf("\n Used pieces record:\n |NUMBER|  |QUANTITY| \n ==========================\n");
                for(i=0;i<temp.p_num;i++)
                {
                    printf(" |  %d   |  |   %d    | \n", temp.used_pieces[i][0],temp.used_pieces[i][1]);
                }
            }

            printf("________________________________________________________________________________");

        }

    }
    else if(choice==3)
    {

        FILE *pp=fopen(adress_p,"rb");
        if(file_is_ok(pp))
        {
            if(count_pieces()==0)
                printf("\nthere is no pieces in the pieces file..!\n");
            else
            {


            printf("Pieces record:\n\n     NUMBER   \t      NAME     \t      PICE    \t     QUANTITY\t\t\n=================================================================\n");
            printf("");
            pieces temp;
            while(fread(&temp,sizeof(pieces),1,pp)!=NULL)
            {
                printf("|\t%d\t|\t%s\t|\t%d\t|\t%d\t|\n",temp.p_n,temp.p_name,temp.p_price,temp.p_sum);
            }
            printf("_________________________________________________________________");
            }
            fclose(pp);


        }


    }


}

int main()
{
    creat_file();
    int num,m_n;
    char choice;
    char d_owner[20];
    date D;
    while(1)
    {
      printf("\nThe list of commands :\n-----------------------\n%c1 Show records.\n\n%c2 Add a car.\n\n%c3 Add a new maintenance process.\n\n%c4 Delete a car.\n\n%c5 Add a new piece.\n\n%c6 Search by the owner.\n\n",16,16,16,16,16,16,16,16);
      printf("%c7 Delete a process.\n\n%c8 Show maintenance for a certain car.\n\n%c9 show cars from a date.\n\n%c(w) type of cars which replaced wheels.\n\n%c(m) Most used piece.\n\n%c(e) Exit.\nwhat would like to do ?\nyour choice : ",16,16,16,16,16);
      fflush(stdin);
      scanf("%c",&choice);
      fflush(stdin);
      printf("-------------------------------------\nThe result :\n\n");
      switch(choice)
      {
      case '1':
          display_file();
          system("pause");
          system("cls");
        break;
      case '2':
          add_a_car();
          system("pause");
          system("cls");
          break;
      case '3':
          add_procces();
          system("pause");
          system("cls");
          break;
      case '4':
         printf(" %c Enter car's number you want to delete:\t",15);
         scanf("%d",&num);
         delete_a_car(num);
         break;
      case '5':
            add_piece();
            break;
      case '6':
          printf(" %c Enter an owner to search:\t",15);
          scanf("%s",&d_owner);
          search_by_owner(d_owner);
          system("pause");
          system("cls");
          break;
      case '7':
          printf("Enter process number:\n");
          scanf("%d",&m_n);
          delete_a_process(m_n);
          system("pause");
          system("cls");
          break;

      case '8':
          printf("Enter car number to show maintenance:\t ");
          scanf("%d",&num);
         display_car_maint(num);
         system("pause");
          system("cls");
         break;
      case '9':
            do
            {printf("enter process date:\n\tday: ");
            scanf("%d",&D.d);
            if((D.d < 1) || (D.d > 31))
                printf("error..! renter the day.\n");
            }
            while((D.d < 1) || (D.d> 31));
            do
            {printf("\n\tmonth: ");
            scanf("%d",&D.m);
            if((D.m < 1) || (D.m > 12))
                printf("error..! reenter the month.\n");
            }
            while((D.m < 1) || (D.m > 12));
            do
            {printf("\n\tyear: ");
            scanf("%d",&D.y);
            if((D.y < 1990) || (D.d > 2018))
                printf("error..! reenter the year.\n");
            }
            while((D.y < 1990) || (D.d > 2018));
            display_car_from__date(D);
            system("pause");
          system("cls");
            break;
      case 'w':
          wheels();
          system("pause");
          system("cls");
          break;

      case 'e':
          printf("\n\athank you for using our software .. %c",3);
         return 0;
      case 'm':
        most_used_piece();
           system("pause");
          system("cls");
        break;
      default:
         printf("\nError : please choose a number from the list of commands\n");
         break;
      }
    }

    return 0;
}
